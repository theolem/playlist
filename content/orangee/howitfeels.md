Title: How It Feels
Authors: Sam Wise
Date: 2017-01-03
Modified: 2020-05-07
Category: Orangée
Link: https://youtu.be/nkUKtE1CD7k

Un rappeur de Londres qui détonne par son côté chaleureux là où ses compères sont plutôt "crachin et doudoune". Son dernier EP "None The Wiser" est aussi très cool.
