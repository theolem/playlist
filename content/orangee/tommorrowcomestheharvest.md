Title: Tomorrow Comes The Harvest
Authors: Tony Allen & Jeff Mills
Date: 2017-01-03
Modified: 2020-05-07
Category: Orangée
Link: https://youtu.be/UoeBCPptWxE

Un mariage improbable entre un producteur de techno de Detroit et le batteur de Fela Kuti, fondateur de l'afrobeat (REP).
