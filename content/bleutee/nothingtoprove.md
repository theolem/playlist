Title: Nothing To Prove
Authors: Kae Tempest
Date: 2017-01-03
Modified: 2020-05-07
Category: Bleutée
Link: https://youtu.be/68FMGZ9DKOU

Tout son dernier album, tout en gros synthés profonds, est très beau. J'avais un peu du mal avec le côté "poésie déclamée" pendant un moment, mais le ton épique de l'album me le fait complètement oublier.
