Title: There'd Better Be A Mirror Ball
Authors: Arctic Monkeys
Date: 2017-01-03
Modified: 2020-05-07
Category: Bleutée
Tags:
Link: https://youtu.be/FY5CAz6S9kE

Des fois je pense que j'aime ce groupe juste pour les titres de leurs chansons. Leur nouvel album est très calme, avec piano et orchestre, j'adore la production.
