Title: Sometimes I Sing The Blues
Authors: Spoek Mathambo
Date: 2017-01-03
Modified: 2020-05-07
Category: Bleutée
Link: https://youtu.be/MBzsunzk9hY

Un album calme et étrange d'un producteur Sud-Africain. Les voix et samples me rendent toujours un peu mélancoliques, un peu comme avec un autre groupe : Space Afrika.
