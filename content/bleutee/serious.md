Title: Serious
Authors: Mansur Brown
Date: 2017-01-03
Modified: 2020-05-07
Category: Bleutée
Link: https://youtu.be/-QljSw8HivU

Énorme guitariste de jazz londonien, avec un jeu très rythmique, qui crée des espaces vaporeux que personne n'avait jamais exploré.
