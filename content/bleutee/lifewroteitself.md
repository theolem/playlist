Title: Life Wrote Itself
Authors: Chester Watson
Date: 2017-01-03
Modified: 2020-05-07
Category: Bleutée
Link: https://youtu.be/c_5rYXz5aRM

Un rappeur de St Louis que j'ai beaucoup écouté en cette fin d'année. L'écart entre son album "Tin Wooki", qui commence déjà à dater, et ce qu'il fait actuellement est assez impressionant : sa voix est plus profonde, sa production plus recherchée, mais son écriture toujours aussi fine et mystérieuse.
