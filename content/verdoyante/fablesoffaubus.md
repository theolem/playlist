Title: Fables of Faubus
Authors: Charles Mingus
Date: 2017-01-03
Modified: 2020-05-07
Category: Verdoyante
Tags:
Link: https://youtu.be/48eAYnfgrAo

Mon standard préféré de tous les temps, qui me fait penser à une BO de vieux film d'espion. Il y a un fond engagé à cette chanson, que je vous laisse découvrir.
