Title: Everybody loves the sunshine
Authors: Takuya Kuroda
Date: 2017-01-03
Modified: 2020-05-07
Category: Verdoyante
Tags:
Link: https://youtu.be/lZ8zu1aRYA8

Version très épurée, qui fait magnifiquement ressortir la voix. Dans le même album, "Rising Son" est une des première chanson que j'ai essayé de jouer à la trompette.
