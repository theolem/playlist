Title: Space Samba
Authors: Lydian Collective
Date: 2017-01-03
Modified: 2020-05-07
Category: Verdoyante
Tags:
Link: https://youtu.be/QzhmE-LHEOw

Le premier album pleinement produit d'un groupe qui s'était surtout fait connaître avec des vidéos intimistes dans leur studio. Ils arrivent parfois à avoir un son de jeu de Gameboy bizarrement addictif.
