Title: Call of the Mountains
Authors: Foehn Trio
Date: 2017-01-03
Modified: 2020-05-07
Category: Verdoyante
Tags:
Link: https://youtu.be/34N_63yglDY

Trio originaire des Alpes mais formé à Lyon. Le pianiste raconte en concert que cette chanson évoque un téléphérique qu'on peut prendre vers Chamonix, et les paysages qui défilent dessous. On les a vu en concert y'a pas longtemps avec Manon, et c'est louuuuurd
