Title: Dribble
Authors: Anomalie
Date: 2017-01-03
Modified: 2020-05-07
Category: Verdoyante
Tags:
Link: https://youtu.be/QLDeUebGpfQ

Ce pianiste canadien a sorti un album fin 2021, qui a fait le fond musical d'une bonne partie de mon année. J'adore la multiplicité des sons et des groove, tout se renouvelle à chaque seconde.
